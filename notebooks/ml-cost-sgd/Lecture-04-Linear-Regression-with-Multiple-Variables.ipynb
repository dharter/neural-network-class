{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import pandas as pd\n",
    "%matplotlib inline\n",
    "plt.rcParams['figure.figsize'] = (8, 6) # set default figure size, 8in by 6in"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Video W2 01: Multiple Features\n",
    "\n",
    "[YouTube Video Link](https://www.youtube.com/watch?v=Zn0u3mKrBEo&list=PLZ9qNFMHZ-A4rycgrgOYma6zxF4BZGGPW&index=19)\n",
    "\n",
    "In the previous week, we only looked at a toy example of linear regression with a single feature, which\n",
    "ended up needing two parameters to be fitted to create a hypothesis or model of the data.\n",
    "\n",
    "The technique that we looked at, and the equations we developed, for linear regression are completely\n",
    "generalizable to as many input features as are available that we would like to fit.\n",
    "\n",
    "Notationally, we had been simply using $x$ to denote the input, and $y$ to denote the output.  Now\n",
    "that we have more features, we will need a bit of additional notation.  Recall that we used $x^{(i)}$\n",
    "to denote the $i^{th}$ input and output training example.  However, now instead of having a single\n",
    "feature (like size in square feet), we have $n$ number of features.  We will use the expanded notation:\n",
    "\n",
    "**Notation:**\n",
    "\n",
    "- $n = $ number of features\n",
    "- $x^{(i)} = $ input (features) of the $i^{th}$ training example (this is a vector of length $n$ ).\n",
    "- $x_j^{(i)} = $ value of particular feature $j$ in the $i^{th}$ training example.\n",
    "- $m = $ number of training examples.\n",
    "- $y^{(i)} = $ output of the $i^{th}$ training example.\n",
    "\n",
    "Using `NumPy` array indexing, the first index will select the row, which corresponds to the input\n",
    "pattern number $i$, and the second index will select the column, which corresponds to the feature\n",
    "number $j$.  Here is an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(47, 4)\n",
      "(47,)\n"
     ]
    }
   ],
   "source": [
    "house = pd.read_csv('data/housing-prices-4-features-portland-or.csv')\n",
    "x = house.ix[:,0:4].as_matrix()\n",
    "y = house.price.as_matrix()\n",
    "\n",
    "print x.shape\n",
    "print y.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[2104    3    1   45]\n",
      " [1600    3    2   40]\n",
      " [2400    3    2   30]\n",
      " [1416    2    1   36]]\n"
     ]
    }
   ],
   "source": [
    "# the first 4 training input patterns\n",
    "print x[:4]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1416    2    1   36]\n"
     ]
    }
   ],
   "source": [
    "# the input training pattern at index 3 (0 based indexing, so actually the 4th overall\n",
    "print x[3]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "size (sq ft.) =  1416\n",
      "num bedrooms  =  2\n",
      "num floors    =  1\n",
      "age of house  =  36\n"
     ]
    }
   ],
   "source": [
    "# feature 0 is the size, feature 1 is the number of bedrooms, feature 2 is number of floors,\n",
    "# feature 3 is the age of the house\n",
    "print \"size (sq ft.) = \", x[3,0]\n",
    "print \"num bedrooms  = \", x[3,1]\n",
    "print \"num floors    = \", x[3,2]\n",
    "print \"age of house  = \", x[3,3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is the form of the hypothesis with more than 1 feature. For 4 features we would have:\n",
    "\n",
    "$$h_\\theta(x) = \\theta_0 + \\theta_1 x_1 + \\theta_2 x_2 + \\theta_3 x_3 + \\theta_4 x_4$$\n",
    "\n",
    "And in general, for $n$ features, we have $n + 1$ parameters:\n",
    "\n",
    "$$h_\\theta(x) = \\theta_0 + \\theta_1 x_1 + \\theta_2 x_2 + ... + \\theta_n x_n$$\n",
    "\n",
    "For convenience in programming, we usually think of the previous equation as having\n",
    "an additional input $x_0 = 1$ that is always set to one.  In this way, we can define\n",
    "$n + 1$ sized vectors to represent some particular set of input features, and another \n",
    "$n + 1$ sized vector to represent a set of $\\theta$ hypothesis parameters:\n",
    "\n",
    "$$\n",
    "x =\n",
    "\\begin{bmatrix}\n",
    "x_0 \\\\\n",
    "x_1 \\\\\n",
    "x_2 \\\\\n",
    "\\vdots \\\\\n",
    "x_n \\\\\n",
    "\\end{bmatrix}\n",
    "\\in \\mathbb{R}^{n+1}\n",
    "\\;\\;\\;\n",
    "\\theta =\n",
    "\\begin{bmatrix}\n",
    "\\theta_0 \\\\\n",
    "\\theta_1 \\\\\n",
    "\\theta_2 \\\\\n",
    "\\vdots \\\\\n",
    "\\theta_n \\\\\n",
    "\\end{bmatrix}\n",
    "\\in \\mathbb{R}^{n+1}\n",
    "$$\n",
    "\n",
    "And a sort of a note: This is convenient for us using a 0 based indexing programming language like \n",
    "`NumPy` arrays, as we can use the $0^{th}$ index for the $x_0 = 1$ and the $\\theta_0$ values.\n",
    "\n",
    "Finally, the full form of our hypothesis for linear regression is:\n",
    "\n",
    "$$h_\\theta(x) = \\theta_0 x_0 + \\theta_1 x_1 + \\theta_2 x_2 + ... + \\theta_n x_n$$\n",
    "\n",
    "Using the transpose of the $\\theta$ vector, we can use simple vector vector multiplication,\n",
    "simplifying the hypothesis equation to be:\n",
    "\n",
    "$$h_\\theta(x) = \\theta^T x$$\n",
    "\n",
    "As an example, lets show how, given a set of 4 feature inputs, and a set of 4 hypothesis parameters,\n",
    "we can get the hypothesis output using this simple vector vector multiplication:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[ 147.63]]\n"
     ]
    }
   ],
   "source": [
    "x = np.array([[1.0], \n",
    "              [1416], \n",
    "              [3],\n",
    "              [2], \n",
    "              [40]])\n",
    "theta = np.array([[80],\n",
    "                  [0.1],\n",
    "                  [0.01],\n",
    "                  [3],\n",
    "                  [-2]])\n",
    "\n",
    "print np.dot(np.transpose(theta), x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This result basically represents the prices (in 1000s of \\$) for the given set of inputs, and given\n",
    "the current set of hypothesis parameters.  Notice that we have 4 input features, and the $x_0$ feature\n",
    "is the dummy value $1.0$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Video W2 02: Gradient Descent for Multiple Variables\n",
    "\n",
    "[YouTube Video Link](https://www.youtube.com/watch?v=mmclAQ-UlbE&list=PLZ9qNFMHZ-A4rycgrgOYma6zxF4BZGGPW&index=20)\n",
    "\n",
    "To summarize, here are our equations and notation for multivariate linear regression that\n",
    "we are currently working with:\n",
    "\n",
    "- **Hypothesis:** $h_\\theta(x) = \\theta^T x = \\theta_0 x_0 + \\theta_1 x_1 + \\theta_2 x_2 + ... + \\theta_n x_n$\n",
    "- **Parameters:** $\\theta_0, \\theta_1, \\dots, \\theta_n$\n",
    "- **Cost function:**\n",
    "$$J(\\theta_0, \\theta_1, \\dots, \\theta_n) = \\frac{1}{2m} \\sum_{i=1}^{m} \\big( h_\\theta(x^{(i)}) - y^{(i)} \\big)^2$$\n",
    "- **Gradient descent:**\n",
    "`repeat` {\n",
    "$$\\theta_j := \\theta_j - \\alpha  \\frac{\\partial}{\\partial \\theta_j} J(\\theta_0,..., \\theta_n)$$\n",
    "} (simultaneously update for every $j = 0, ..., n$\n",
    "\n",
    "The cost function allows you to calculate the \"cost\", or how well a particular set of hypothesis\n",
    "parameters $\\theta$ does in modeling the data.  The lower the cost, the better the parameters are at\n",
    "fitting the data.  The cost function sums differences squared between the hypothes model output\n",
    "and the actual output.\n",
    "\n",
    "The gradient descent algorithm is an algorithm we can use to find the values of the $\\theta$ parameters\n",
    "that give the best, or minimum cost, when fitting to the data.  When we were looking at 1 variable\n",
    "last week, we only had two theta parameters.  For the multivariate case, we have to simultaneously\n",
    "update the $n + 1$ $\\theta$ parameters, but this extension to the basic idea is straight forward.\n",
    "\n",
    "## Gradient Descent update Equations\n",
    "\n",
    "The generalized case for mutivariate gradient descent looks like the following.  This equation\n",
    "basically has determined the partial derivative of the $J()$ cost function with respect to each\n",
    "variable:\n",
    "\n",
    "\n",
    "`repeat` {\n",
    "$$\\theta_j := \\theta_j - \\alpha  \\frac{1}{m} \\sum_{i=1}^m \\big(h_\\theta(x^{(i)}) - y^{(i)} \\big) x_j^{(i)} $$\n",
    "} (simultaneously update for every $j = 0, ..., n$\n",
    "\n",
    "\n",
    "Two things to note about this form for our gradient descent algorithm:\n",
    "\n",
    "1. $x_0$ will always be 1 by definition, so this form reduces to the form we gave in the univariate \n",
    "   case.\n",
    "2. The partial derivative becomes the result of a summation of the difference between the\n",
    "   hypothesis output and the actual output (divided by $m$, so you get an average over all\n",
    "   $m$ input patterns), multiplied by the particular value of the feature for each pattern.  The\n",
    "   only thing that differs in each case is the particular feature, which is a result of taking\n",
    "   the partial derivative with respect to that feature.\n",
    "   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Video W2 03: Gradient Descent in practice I: feature scaling\n",
    "\n",
    "[YouTube Video Link](https://www.youtube.com/watch?v=yQci-wS0iMw&list=PLZ9qNFMHZ-A4rycgrgOYma6zxF4BZGGPW&index=21)\n",
    "\n",
    "Scaling features can help make sure gradient descent converges at a much faster pace.  Generally\n",
    "scaling means in practice we will try and get all features to be in the range $-1 \\le x_i \\le 1$.\n",
    "\n",
    "Scaling can be done in 2 steps:\n",
    "\n",
    "1. Center the data (mean normalization), so features have approximately have 0 mean.\n",
    "2. Scale the data to a common range (scaling).\n",
    "\n",
    "Here is a quick example for our house data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "house = pd.read_csv('data/housing-prices-4-features-portland-or.csv')\n",
    "house.insert(0, 'default', '1.0') # insert column at index 0, the 1.0 default values\n",
    "x = house.ix[:,0:5].as_matrix()\n",
    "y = house.price.as_matrix()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2104 1600 2400 1416 3000 1985 1534 1427 1380 1494 1940 2000 1890 4478 1268\n",
      " 2300 1320 1236 2609 3031 1767 1888 1604 1962 3890 1100 1458 2526 2200 2637\n",
      " 1839 1000 2040 3137 1811 1437 1239 2132 4215 2162 1664 2238 2567 1200 852\n",
      " 1852 1203]\n",
      "Mean before centering: 2000.68085106\n",
      "\n",
      "[103.31914893617022 -400.6808510638298 399.3191489361702 -584.6808510638298\n",
      " 999.3191489361702 -15.680851063829778 -466.6808510638298\n",
      " -573.6808510638298 -620.6808510638298 -506.6808510638298\n",
      " -60.68085106382978 -0.6808510638297776 -110.68085106382978\n",
      " 2477.31914893617 -732.6808510638298 299.3191489361702 -680.6808510638298\n",
      " -764.6808510638298 608.3191489361702 1030.3191489361702\n",
      " -233.68085106382978 -112.68085106382978 -396.6808510638298\n",
      " -38.68085106382978 1889.3191489361702 -900.6808510638298\n",
      " -542.6808510638298 525.3191489361702 199.31914893617022 636.3191489361702\n",
      " -161.68085106382978 -1000.6808510638298 39.31914893617022\n",
      " 1136.3191489361702 -189.68085106382978 -563.6808510638298\n",
      " -761.6808510638298 131.31914893617022 2214.31914893617 161.31914893617022\n",
      " -336.6808510638298 237.31914893617022 566.3191489361702 -800.6808510638298\n",
      " -1148.6808510638298 -148.68085106382978 -797.6808510638298]\n",
      "Mean after centering: 9.67547555078e-15\n"
     ]
    }
   ],
   "source": [
    "# mean normalization, center the data around 0\n",
    "\n",
    "# the house size data is in x[:,1]\n",
    "print x[:,1]\n",
    "\n",
    "# the mean value of the house data is\n",
    "print \"Mean before centering:\", np.mean(x[:,1])\n",
    "print \"\"\n",
    "\n",
    "# do the actual centering\n",
    "x[:,1] = x[:,1] - np.mean(x[:,1])\n",
    "\n",
    "print x[:,1]\n",
    "print \"Mean after centering:\", np.mean(x[:,1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3626.0\n",
      "[0.028493973782727586 -0.11050216521341141 0.11012662684395208\n",
      " -0.161246787386605 0.27559822088697467 -0.004324559035805234\n",
      " -0.1287040405581439 -0.15821314149581628 -0.17117508302918638\n",
      " -0.13973548016101206 -0.01673492858903193 -0.0001877691847296684\n",
      " -0.030524228092617147 0.6832099142129537 -0.20206311391721726\n",
      " 0.08254802783678164 -0.18772224243348865 -0.2108882655995118\n",
      " 0.1677658987689383 0.28414758657919753 -0.06444590487143678\n",
      " -0.031075800072760555 -0.10939902125312459 -0.010667636807454434\n",
      " 0.5210477520507916 -0.24839516024926359 -0.14966377580359344\n",
      " 0.14487566159298682 0.0549694288296112 0.175487906490946\n",
      " -0.04458931358627407 -0.275973759256434 0.010843670418138506\n",
      " 0.3133809015267982 -0.05231132130828179 -0.15545528159509922\n",
      " -0.2100609076292967 0.0362159815047353 0.6106781988240955\n",
      " 0.044489561206886435 -0.09285186184882233 0.06544929645233596\n",
      " 0.1561828871859267 -0.22081656124209315 -0.31679008578704626\n",
      " -0.041004095715341915 -0.21998920327187804]\n"
     ]
    }
   ],
   "source": [
    "# scaling, the data ranges from the minimum value up to the maximum value, if we divide by\n",
    "# this range, we will scale the data to values between -1.0 and 1.0\n",
    "minimum = min(x[:,1])\n",
    "maximum = max(x[:,1])\n",
    "rnge = maximum - minimum\n",
    "print rnge\n",
    "\n",
    "x[:,1] = x[:,1] / rnge\n",
    "print x[:,1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Video W2 04: Gradient Descent in practice II: Learning Rate\n",
    "\n",
    "[YouTube Video Link](https://www.youtube.com/watch?v=mlHcA-VCyN0&list=PLZ9qNFMHZ-A4rycgrgOYma6zxF4BZGGPW&index=22)\n",
    "\n",
    "The take away from this video discussion:\n",
    "\n",
    "- If $\\alpha$ is too small: slow convergence\n",
    "- If $\\alpha$ is too large: $J(\\theta)$ may not decrease on every iteration; may not converge.\n",
    "\n",
    "In practice, try running with a range of $\\alpha$ learning rate values and plot the cost as a function\n",
    "of the number of iterations.  You want to find values for the learning rate where the cost converges\n",
    "and is reduced at a good rate.\n",
    "\n",
    "# Video W2 05: Features and Polynomial Regression\n",
    "\n",
    "[YouTube Video Link](https://www.youtube.com/watch?v=8p3S9aR3n4o&index=23&list=PLZ9qNFMHZ-A4rycgrgOYma6zxF4BZGGPW)\n",
    "\n",
    "Some discussions here on how you can make linear regression more powerful.  The main idea is that\n",
    "you can actually combine or choose features.  This does not effect the cost function nor calculating\n",
    "gradient descent, but it can sometimes be used to find models for data that have complex, nonlinear\n",
    "features.\n",
    "\n",
    "# Video W2 06: Normal Equation\n",
    "\n",
    "[YouTube Video Link](https://www.youtube.com/watch?v=rnlti7rgns0&list=PLZ9qNFMHZ-A4rycgrgOYma6zxF4BZGGPW&index=24)\n",
    "\n",
    "The normal equation is basically an analytical solution to solve for the optimal $\\theta$ parameters.\n",
    "It has some advantages and disadvantages over using the gradient descent algorithm.  Here is the\n",
    "normal equation:\n",
    "\n",
    "$$\n",
    "\\theta = (X^T X)^{-1} X^T y\n",
    "$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[  4.55773438e+02]\n",
      " [  4.09240723e-01]\n",
      " [ -1.57625000e+02]\n",
      " [ -8.37187500e+01]\n",
      " [ -5.74609375e+00]]\n"
     ]
    }
   ],
   "source": [
    "X = np.array([[1.0, 2104, 5, 1, 45],\n",
    "              [1.0, 1416, 3, 2, 40],\n",
    "              [1.0, 1534, 3, 2, 30],\n",
    "              [1.0,  852, 2, 1, 36]])\n",
    "\n",
    "y = np.array([[460],\n",
    "              [232],\n",
    "              [315],\n",
    "              [178]])\n",
    "\n",
    "# these lines implement (X^T X)^-1 X^T y\n",
    "# this is the Normal Equation, the values given for Theta are the minimum\n",
    "# fitted parameters for the given data\n",
    "Tmp = np.linalg.inv(np.dot(X.T, X))\n",
    "Theta = np.dot(np.dot(Tmp, X.T), y)\n",
    "print Theta"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This method is basically the same as solving a series of simultaneous equations, where you set each\n",
    "equation to 0.  \n",
    "\n",
    "What are the advantages of and disadvantates of gradient descent vs. solving analytically using the\n",
    "normal equation?\n",
    "\n",
    "Gradient descent requires you to figure out a good $\\alpha$ learning rate, and can require many iterations\n",
    "to converge (so might take some time).  The normal equation does not have any parameter like the learning\n",
    "rate that you have to figure out, and it does not require any iterations.\n",
    "\n",
    "However, the need to compute the inverse of a matrix is itself a very time costly algorithm for a computer, so for very large numbers of features, inverting the matrix might end up taking much longer\n",
    "than gradient descent.  So gradient descent works well, even if you have very large numbers of features.\n",
    "Thus for Big Data problems, that can have thousands or millions of features, you almost always use the\n",
    "iterative gradient descent."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Video W2 07: Normal Equation Noninvertibility (Optional)\n",
    "\n",
    "[YouTube Video Link](https://www.youtube.com/watch?v=t-5Take484A&list=PLZ9qNFMHZ-A4rycgrgOYma6zxF4BZGGPW&index=25)\n",
    "\n",
    "Only some matrices are invertible.  Those that are not invertible are called singular or degenerate\n",
    "matrices.  Intuitively, you can think of these matrices as being in some sense close to the value 0. \n",
    "The scalar value is not invertible, and such degenerate matrices are not invertible for a similar reason.\n",
    "Common causes for a matrix to be non-invertible are:\n",
    "\n",
    "1. Redundant features (that are linearly dependent)\n",
    "2. Too many features (e.g. $m \\le n$)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "application/json": {
       "Software versions": [
        {
         "module": "Python",
         "version": "2.7.13 64bit [GCC 6.3.0 20170118]"
        },
        {
         "module": "IPython",
         "version": "5.1.0"
        },
        {
         "module": "OS",
         "version": "Linux 4.10.0 30 generic x86_64 with Ubuntu 17.04 zesty"
        },
        {
         "module": "numpy",
         "version": "1.12.1"
        },
        {
         "module": "scipy",
         "version": "0.18.1"
        },
        {
         "module": "matplotlib",
         "version": "2.0.0"
        },
        {
         "module": "pandas",
         "version": "0.19.2"
        },
        {
         "module": "sklearn",
         "version": "0.18"
        }
       ]
      },
      "text/html": [
       "<table><tr><th>Software</th><th>Version</th></tr><tr><td>Python</td><td>2.7.13 64bit [GCC 6.3.0 20170118]</td></tr><tr><td>IPython</td><td>5.1.0</td></tr><tr><td>OS</td><td>Linux 4.10.0 30 generic x86_64 with Ubuntu 17.04 zesty</td></tr><tr><td>numpy</td><td>1.12.1</td></tr><tr><td>scipy</td><td>0.18.1</td></tr><tr><td>matplotlib</td><td>2.0.0</td></tr><tr><td>pandas</td><td>0.19.2</td></tr><tr><td>sklearn</td><td>0.18</td></tr><tr><td colspan='2'>Fri Aug 25 11:11:34 2017 CDT</td></tr></table>"
      ],
      "text/latex": [
       "\\begin{tabular}{|l|l|}\\hline\n",
       "{\\bf Software} & {\\bf Version} \\\\ \\hline\\hline\n",
       "Python & 2.7.13 64bit [GCC 6.3.0 20170118] \\\\ \\hline\n",
       "IPython & 5.1.0 \\\\ \\hline\n",
       "OS & Linux 4.10.0 30 generic x86\\_64 with Ubuntu 17.04 zesty \\\\ \\hline\n",
       "numpy & 1.12.1 \\\\ \\hline\n",
       "scipy & 0.18.1 \\\\ \\hline\n",
       "matplotlib & 2.0.0 \\\\ \\hline\n",
       "pandas & 0.19.2 \\\\ \\hline\n",
       "sklearn & 0.18 \\\\ \\hline\n",
       "\\hline \\multicolumn{2}{|l|}{Fri Aug 25 11:11:34 2017 CDT} \\\\ \\hline\n",
       "\\end{tabular}\n"
      ],
      "text/plain": [
       "Software versions\n",
       "Python 2.7.13 64bit [GCC 6.3.0 20170118]\n",
       "IPython 5.1.0\n",
       "OS Linux 4.10.0 30 generic x86_64 with Ubuntu 17.04 zesty\n",
       "numpy 1.12.1\n",
       "scipy 0.18.1\n",
       "matplotlib 2.0.0\n",
       "pandas 0.19.2\n",
       "sklearn 0.18\n",
       "Fri Aug 25 11:11:34 2017 CDT"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "%load_ext version_information\n",
    "\n",
    "%version_information numpy, scipy, matplotlib, pandas, sklearn"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
