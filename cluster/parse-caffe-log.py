#!/usr/bin/env python
"""
parse-caffe-log.py: parse a Caffe deep neural network training log file.
Output results as a table to standard output of the traing loss and test
accuracy data.
"""
import argparse
import re
import fileinput
import sys


def name_regex(name, pattern):
    """Return regex string as a named capture group."""
    return r'(?P<{name}>{pattern})'.format(name=name, pattern=pattern)


def named_regexes(**names_and_patterns):
    """Return dictionary with regexes transformed into named catpure groups."""
    return {k : name_regex(k, p)
              for k, p in names_and_patterns.items()}


def parse_training_performance(log_file_name):
    """This function parses a Caffe log file for training performance
    statistics.  The result is written to standard output in the form
    of a comma separated values (csv) table
    """
    # patterns we will pull out of parsed log file lines
    rx_patterns = named_regexes(
        iteration      = r'\d+',
        loss           = r'\d+[\.\d+]*[e\-\d+]*',
        iter_per_sec   = r'\d+[\.\d+]*',
        sec_per_iter   = r'\d+[\.\d+]*',
        num_iter_timed = r'\d+',
        timestamp      = r'\d{2}:\d{2}:\d{2}\.\d{6}',
        learning_rate  = r'\d+[\.\d+]*[e\-\d+]*',
    )

    # other useful patterns, don't need to be parsed
    rx_patterns['skip'] = r'.*'

    # first training result line, get iteration number, loss and timing performance stats from this line
    rx_train_line1 = "^{skip}{timestamp}{skip}Iteration {iteration}, loss = {loss}$".format(**rx_patterns)
    rx_train_line1_wtime = "^{skip}{timestamp}{skip}Iteration {iteration} \({iter_per_sec} iter/s, {sec_per_iter}s/{num_iter_timed} iters\), loss = {loss}$".format(**rx_patterns)

    # second training result line, simply geting the current learning rate
    rx_train_line2 = "^{skip}{timestamp}{skip}Iteration {iteration}, lr = {learning_rate}$".format(**rx_patterns)

    # print out a header
    sys.stdout.write("iteration,loss,timestamp,iter_per_sec,sec_per_iter,num_iter_timed,learning_rate\n")

    # parse log file looking for lines relating to training learning performance
    for line in fileinput.input(log_file_name):
        # match first training performance line
        match = re.search(rx_train_line1, line)
        if match:
            d = match.groupdict()
            iteration = int(d['iteration'])
            loss = float(d['loss'])
            iter_per_sec = 'NA'
            sec_per_iter = 'NA'
            num_iter_timed = 'NA'
            timestamp = d['timestamp']
            sys.stdout.write("%d,%f,%s,%s,%s,%s," % (iteration, loss, timestamp, iter_per_sec, sec_per_iter, num_iter_timed) )

        match = re.search(rx_train_line1_wtime, line)
        if match:
            d = match.groupdict()
            iteration = int(d['iteration'])
            loss = float(d['loss'])
            iter_per_sec = float(d['iter_per_sec'])
            sec_per_iter = float(d['sec_per_iter'])
            num_iter_timed = int(d['num_iter_timed'])
            timestamp = d['timestamp']
            sys.stdout.write("%d,%f,%s,%f,%f,%d," % (iteration, loss, timestamp, iter_per_sec, sec_per_iter, num_iter_timed) )

        # match second training performance line
        match = re.search(rx_train_line2, line)
        if match:
            d = match.groupdict()
            learning_rate = float(d['learning_rate'])
            sys.stdout.write("%f\n" % learning_rate)


def parse_test_performance(log_file_name):
    """This function parses a Caffe log file for test performance
    statistics.  The results are produced on standard output in
    the form of a comman separated values (csv) table of statistics.
    """
    # patterns we will pull out of parsed log file lines
    rx_patterns = named_regexes(
        iteration = r'\d+',
        accuracy  = r'\d+\.\d+',
        loss      = r'\d+\.\d+',
        timestamp = r'\d{2}:\d{2}:\d{2}\.\d{6}',
    )

    # other useful patterns, don't need to be parsed
    rx_patterns['skip'] = r'.*'

    # zeroth test result line, simply get which iteration we are on that we tested the performance against
    rx_test_line0 = "^{skip}Iteration {iteration}, Testing net \(#0\)$".format(**rx_patterns)

    # first test result line, get accuracy performance
    rx_test_line1 = "^{skip}{timestamp}{skip}Test net output #0: accuracy = {accuracy}$".format(**rx_patterns)

    # second test result line, get loss performance
    rx_test_line2 = "^{skip}{timestamp}{skip}Test net output #1: loss = {loss}{skip}$".format(**rx_patterns)

    # print out a header
    sys.stdout.write("iteration,accuracy,loss,timestamp\n")

    # parse log file looking for lines relating to testing data learning performance
    for line in fileinput.input(log_file_name):
        # match zeroth test performance line
        match = re.search(rx_test_line0, line)
        if match:
            d = match.groupdict()
            iteration = int(d['iteration'])
            sys.stdout.write("%d," % iteration)

        # match first test performance line
        match = re.search(rx_test_line1, line)
        if match:
            d = match.groupdict()
            accuracy = float(d['accuracy'])
            sys.stdout.write("%f," % accuracy)

        # match second test performance line
        match = re.search(rx_test_line2, line)
        if match:
            d = match.groupdict()
            loss = float(d['loss'])
            timestamp = d['timestamp']
            sys.stdout.write("%f,%s\n" % (loss, timestamp) )


def main():
    description = """Parse the contents of a Caffee training and testing log file.
    This script can parse out either training or testing results
    from the log file, such as loss and accuracy for each iteration
    of training, as well as other timing information.  This script
    formats the results as a table, suitable for graphing or other
    purposes.
    """
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("caffe_logfile", help="The caffe log file to parse performance information from.")
    parser.add_argument("-t", "--train", help="Parse and produce training performance data", action="store_true")
    parser.add_argument("-s", "--test", help="Parse and produce testing performance data", action="store_true")
    args = parser.parse_args()

    if not args.train and not args.test:
        parser.print_help()

    if args.train:
        parse_training_performance(args.caffe_logfile)
    if args.test:
        parse_test_performance(args.caffe_logfile)


if __name__ == '__main__':
    main()
